// Aula sobre variáveis
console.log("Variáveis");

var nome = "Milena";
var cidade = "Campo Mourão";
console.log("Meu nome é " + nome);
console.log("Sou a " + nome + " e moro em " + cidade);

console.log(typeof nome); //typeof retorna o tipo da variável
nome = 25;
console.log(typeof nome);
console.log(".");

console.log("=========ATIVIDADE=========");

var n1 = 10;
var n2 = 2;
var n3 = 5;
var soma = n1 + n2 + n3;
console.log("1a. Soma = " + soma);

var subtração = n1 - n2 - n3;
console.log("1b. Subtração = " + subtração);

var multiplicação = n1 * n2 * n3;
console.log("1c. Multiplicação = " + multiplicação);

var divisão = n1 / n2 / n3;
console.log("1d. Divisão = " + divisão);

console.log(".");

var L1 = 1;
var L2 = 2;
var área = L1 * L2;
var perimetro = L1 + L1 + L2 + L2;
console.log("2a. Área do retângulo = " + área + " / Perimetro = " + perimetro);

var P_quadrado = L1 * 4;
console.log("2b. Área do quadrado = " + área + " / Perimetro = " + P_quadrado);

var r = 2;
var A_círculo = 3.14 * r ** 2;
var P_círculo = 2 * 3.14 * r;
console.log(
  "2c. Área do círculo = " + A_círculo + " / Perimetro = " + P_círculo
);

var b = 2;
var h = 4;
var L = 2;
var A_triângulo = (b * h) / 2;

var P_triângulo = L * 3;
console.log(
  "2d. Área do triângulo = " + A_triângulo + " / Perimetro = " + P_triângulo
);

console.log(".");

var altura = (L * Math.sqrt(3)) / 2;
console.log("3. Altura do triângulo equilátero = " + altura);

console.log(".");

var nota1 = 73;
var nota2 = 65;
var nota3 = 91;
var nota4 = 80;
var média = nota1 + nota2 + nota3 + nota4 / 4;
console.log("4. Média do aluno = " + média);

console.log(".");

var peso1 = 80 * 1;
var peso2 = 70 * 2;
var peso3 = 40 * 3;
var peso4 = 90 * 4;
var soma_pesos = peso1 + peso2 + peso3 + peso4;
var média_ponderada = soma_pesos / 10;
console.log("5. Média Ponderada = " + média_ponderada);

console.log("=========FIM DA ATIVIDADE=========");

